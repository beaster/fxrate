package me.forex.ecb;

import android.app.Application;
import android.content.Context;

/**
 * Created by Dmitry on 26.10.2016.
 */

public class FxApplication extends Application {

    private static Context sAppContext;

    @Override
    public void onCreate() {
        super.onCreate();
        sAppContext = this;
    }

    public static final Context getAppContext() {
        return sAppContext;
    }
}
