package me.forex.ecb.api;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Dmitry on 26.10.2016.
 */

public interface CurrencyClient {

    @GET("/stats/eurofxref/eurofxref-daily.xml")
    Call<String> getRates();
}
