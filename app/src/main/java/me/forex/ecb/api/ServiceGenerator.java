package me.forex.ecb.api;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

/**
 * Created by Dmitry on 18.09.2016.
 */

public class ServiceGenerator {

    public static final String API_BASE_URL = "http://www.ecb.europa.eu/";

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .addConverterFactory(new ToStringConverterFactory())
                    .baseUrl(API_BASE_URL);

    public static <S> S createService(Class<S> serviceClass) {
        Retrofit retrofit = builder.client(httpClient.build()).build();
        return retrofit.create(serviceClass);
    }

}
