package me.forex.ecb.conversion;

import java.math.BigDecimal;

import me.forex.ecb.data.CurrencyUnit;

/**
 * Created by Dmitry on 28.10.2016.
 * Class to describe conversion rate
 */

public class ConversionRate {

    private CurrencyUnit from;
    private CurrencyUnit to;
    private BigDecimal rate;

    public ConversionRate(CurrencyUnit from, CurrencyUnit to, BigDecimal rate) {
        this.from = from;
        this.to = to;
        this.rate = rate;
    }

    public CurrencyUnit getFrom() {
        return from;
    }

    public CurrencyUnit getTo() {
        return to;
    }

    public BigDecimal getRate() {
        return rate;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(from.getSymbol()).append("1 = ")
                .append(to.getSymbol())
                .append(String.format("%.4f", rate.doubleValue()));

        return sb.toString();
    }
}
