package me.forex.ecb.conversion;

import android.content.Context;
import android.text.TextUtils;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import me.forex.ecb.FxApplication;
import me.forex.ecb.data.CurrencyUnit;
import me.forex.ecb.utils.CurrencyUtil;

/**
 * Created by Dmitry on 26.10.2016.
 *
 * ConversionRateManager to load/save rates form ECB. These rates use EUR as reference currency.
 */

public class ConversionRateManager {

    private final static String FX_RATES_FILE_NAME = "fx_ecb_rates";

    private Conversion mCurrentConversion;

    private final Object mMonitor = new Object();

    public final static class Conversion {

        private Map<String, BigDecimal> rates = new HashMap<>();
        private Date updateDate;

        private String parseErrorMsg;

        /**
         * Date of receive rates
         */
        public Date getUpdateDate() {
            return updateDate;
        }

        /**
         * Get rate of currency
         *
         * @param from
         * @param to
         * @return Current rate
         */
        public ConversionRate getRate(CurrencyUnit from, CurrencyUnit to) {
            CurrencyUtil.checkNull(from, "Currency can't be null");
            CurrencyUtil.checkNull(to, "Currency can't be null");

            BigDecimal formRate = rates.get(from.getCurrencyCode());
            BigDecimal toRate = rates.get(to.getCurrencyCode());

            return new ConversionRate(from, to, toRate.divide(formRate, 5, BigDecimal.ROUND_HALF_UP));
        }

        void putRate(String currency, BigDecimal rate) {
            rates.put(currency, rate);
        }

        void setUpdateDate(Date date) {
            this.updateDate = date;
        }


        /**
         * Parse XML with exchange rates.
         *
         * @throws ParseException If XML can't be parsed.
         */
        void parse(String source) throws ParseException {
            try {
                SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
                SAXParser newSAXParser = saxParserFactory.newSAXParser();
                XMLReader saxReader = newSAXParser.getXMLReader();
                DefaultHandler handler = new DefaultHandler() {

                    public void startElement(String uri, String localName,
                                             String qName, Attributes attributes) {
                        if (localName.equals("Cube")) {
                            String date = attributes.getValue("time");
                            if (date != null) {
                                SimpleDateFormat df = new SimpleDateFormat(
                                        "yyyy-MM-dd");
                                try {
                                    setUpdateDate(df.parse(date));
                                    putRate("EUR", BigDecimal.valueOf(1));
                                } catch (ParseException e) {
                                    parseErrorMsg = "Can't parse date: " + date;
                                }
                            }
                            String currency = attributes.getValue("currency");
                            String rate = attributes.getValue("rate");
                            if (currency != null && rate != null && getUpdateDate() != null) {
                                try {
                                    putRate(currency, new BigDecimal(rate));
                                } catch (Exception e) {
                                    parseErrorMsg = "Can't parse exchange rate: "
                                            + rate + ". " + e.getMessage();
                                }
                            }
                        }
                    }
                };

                saxReader.setContentHandler(handler);
                saxReader.setErrorHandler(handler);

                InputSource inStream = new InputSource();
                inStream.setCharacterStream(new StringReader(source));
                saxReader.parse(inStream);
            } catch (Exception e) {
                parseErrorMsg = "Parser error: " + e.getMessage();
            }
            if (parseErrorMsg != null) {
                throw new ParseException(parseErrorMsg, 0);
            }
        }

    }

    ConversionRateManager() {

    }

    /**
     * Update current exchange rates if needed.
     *
     * @param source - source of rates (XML)
     */
    boolean updateRates(String source) {
        return updateRates(source, true);
    }

    private boolean updateRates(String source, boolean needSave) {
        if (TextUtils.isEmpty(source)) {
            return false;
        }

        Conversion todayRate = new Conversion();
        try {
            todayRate.parse(source);
            boolean hasNewRates = mCurrentConversion == null
                    || mCurrentConversion.getUpdateDate().before(todayRate.getUpdateDate());
            if (hasNewRates) {
                synchronized (mMonitor) {
                    mCurrentConversion = todayRate;
                }
                if (needSave) {
                    saveSourceRates(source);
                }
                return true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Restore state from file
     */
    void restore() {
        updateRates(readFromFile(FxApplication.getAppContext()), false);
    }

    /**
     * Get rate of day
     */
    public Conversion getCurrentRate() {
        synchronized (mMonitor) {
            return mCurrentConversion;
        }
    }

    public boolean hasDataRate() {
        return getCurrentRate() != null;
    }

    /**
     * Save current source rates (XML) to be able to work offline
     */
    private void saveSourceRates(String source) {
        Context context = FxApplication.getAppContext();
        writeToFile(source, context);
    }

    private void writeToFile(String data, Context context) {
            try {
                FileOutputStream outputStream = context.openFileOutput(FX_RATES_FILE_NAME,
                        Context.MODE_PRIVATE);
                outputStream.write(data.getBytes("utf-8"));
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    private String readFromFile(Context context) {
        try {
            BufferedInputStream fis = new BufferedInputStream(context.openFileInput(FX_RATES_FILE_NAME));
            StringBuffer fileContent = new StringBuffer("");
            byte[] buffer = new byte[1024];
            int n;
            while ((n = fis.read(buffer)) != -1) {
                fileContent.append(new String(buffer, 0, n, "utf-8"));
            }
            fis.close();
            return fileContent.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

}
