package me.forex.ecb.conversion;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.Process;

import java.io.IOException;

import me.forex.ecb.api.CurrencyClient;
import me.forex.ecb.api.ServiceGenerator;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Dmitry on 29.10.2016.
 * Helper to sync rate with server
 */

public final class ConversionSyncHelper {

    public interface OnUpdateRateListener {
        void onUpdate(ConversionRateManager.Conversion rate);
    }

    private final static long TIME_TO_UPDATE = 30_000; //30 sec

    private final static int WHAT_INIT = 1;
    private final static int WHAT_UPDATE = 2;

    private class SyncHandler extends Handler {

        SyncHandler(Looper looper) {
            super(looper);
        }

        SyncHandler() {
            super();
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case WHAT_INIT:
                    mRateManager.restore();
                    notifyUpdate();
                    break;
                case WHAT_UPDATE:
                    update();
                    synchronized (mMonitor) {
                        if (mStartSync) {
                            sendMessageDelayed(Message.obtain(this, WHAT_UPDATE), TIME_TO_UPDATE);
                        }
                    }
                    break;
            }
        }

        private void update() {
            CurrencyClient client = ServiceGenerator.createService(CurrencyClient.class);
            Call<String> call = client.getRates();
            try {
                Response<String> response = call.execute();
                if (response != null) {
                    boolean hasNewRates = mRateManager.updateRates(response.body());
                    if (hasNewRates) {
                        notifyUpdate();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Monitor to avoid infinity sync
     */
    private final Object mMonitor = new Object();

    private Handler mHandler;
    private Handler mUiHandler = new Handler(Looper.getMainLooper());
    private ConversionRateManager mRateManager;
    private OnUpdateRateListener mListener;
    private boolean mStartSync;

    private HandlerThread mHandlerThread;

    public ConversionSyncHelper() {
        mRateManager = new ConversionRateManager();
        create();
    }

    private void create() {
        mHandlerThread = new HandlerThread("RateSync", Process.THREAD_PRIORITY_BACKGROUND);
        mHandlerThread.start();
        mHandler = new SyncHandler(mHandlerThread.getLooper());
    }

    private void destroy() {
        mHandlerThread.quit();
    }

    private void notifyUpdate() {
        mUiHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mListener != null && mRateManager.hasDataRate()) {
                    mListener.onUpdate(mRateManager.getCurrentRate());
                }
            }
        });
    }

    public void startSync() {
        mHandler.sendMessage(Message.obtain(mHandler, WHAT_INIT));
        synchronized (mMonitor) {
            if (!mHandler.hasMessages(WHAT_UPDATE)) {
                mHandler.sendMessage(Message.obtain(mHandler, WHAT_UPDATE));
            }
            mStartSync = true;
        }
    }

    public void stopSync() {
        synchronized (mMonitor) {
            mHandler.removeMessages(WHAT_UPDATE);
            mStartSync = false;
        }
    }

    /**
     * Set listener to notify update rate
     */
    public void setOnRateUpdateListener(OnUpdateRateListener listener) {
        mListener = listener;
    }

    /**
     * Get current RateManager to get rate
     */
    public ConversionRateManager getRateManager() {
        return mRateManager;
    }

}
