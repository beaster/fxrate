package me.forex.ecb.data;

import java.util.Currency;
import java.util.Locale;

/**
 * Created by Dmitry on 26.10.2016.
 */

public enum CurrencyUnit {

    USD("USD", Locale.US),
    GBP("GBP", Locale.UK),
    EUR("EUR", Locale.GERMANY);

    private String mCurrencyCode;
    private Locale mLocale;

    CurrencyUnit(String currencyCode, Locale locale) {
        mCurrencyCode = currencyCode;
        mLocale = locale;
    }

    public String getSymbol() {
        return Currency.getInstance(mCurrencyCode).getSymbol(mLocale);
    }

    public String getCurrencyCode() {
        return mCurrencyCode;
    }

    public int getDefaultFractionDigits() {
        return Currency.getInstance(mCurrencyCode).getDefaultFractionDigits();
    }

    //intrinsic
    public double getMultiplier() {
        return Math.pow(10.0, getDefaultFractionDigits());
    }

    @Override
    public String toString() {
        return name();
    }
}
