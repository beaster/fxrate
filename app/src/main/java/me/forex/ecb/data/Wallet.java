package me.forex.ecb.data;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import me.forex.ecb.FxApplication;
import me.forex.ecb.data.money.Money;
import me.forex.ecb.data.money.MoneyFactory;
import me.forex.ecb.utils.CurrencyUtil;

/**
 * Created by Dmitry on 29.10.2016.
 * <p>
 * Wallet to keep money
 */

public class Wallet {

    private static class Instance {
        public static final Wallet sInstance = new Wallet();
    }

    public static Wallet getDefault() {
        return Instance.sInstance;
    }

    private List<Money> mMoneyList = new ArrayList<>();

    private final static List<Money> DEFAULT_WALLET = new ArrayList<Money>() {
        {
            add(MoneyFactory.from("25", CurrencyUnit.USD));
            add(MoneyFactory.from("25", CurrencyUnit.EUR));
            add(MoneyFactory.from("25", CurrencyUnit.GBP));
        }
    };

    private Wallet() {
        restore(mMoneyList);
        if (mMoneyList.isEmpty()) {
            mMoneyList.addAll(DEFAULT_WALLET);
        }
    }

    public void changeMoney(Money money) {
        int pos = mMoneyList.indexOf(money);
        if (pos != -1) {
            mMoneyList.set(pos, money);
        }
    }

    public void addMoney(Money money) {
        CurrencyUtil.checkNull(money, "Money can't be null");
        mMoneyList.add(money);
    }

    public List<Money> getMoneyList() {
        return Collections.unmodifiableList(mMoneyList);
    }

    public void save() {
        save(mMoneyList);
    }

    private void save(List<Money> wallet) {
        if (!wallet.isEmpty()) {
            SharedPreferences.Editor editor = getPreference().edit();
            for (Money money : wallet) {
                editor.putString(money.getCurrency().getCurrencyCode(), money.getAmountString());
            }
            editor.commit();
        }
    }

    private void restore(List<Money> wallet) {
        SharedPreferences pref = getPreference();
        Map<String, ?> moneyMap = pref.getAll();

        if (moneyMap != null && !moneyMap.isEmpty()) {
            for (String key : moneyMap.keySet()) {
                String amount = pref.getString(key, null);
                if (amount != null) {
                    wallet.add(MoneyFactory.from(amount, CurrencyUnit.valueOf(key)));
                }
            }
        }
    }

    private SharedPreferences getPreference() {
        Context ctx = FxApplication.getAppContext();
        return ctx.getSharedPreferences("wallet", Context.MODE_PRIVATE);
    }

}
