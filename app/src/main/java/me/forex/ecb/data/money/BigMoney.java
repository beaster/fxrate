package me.forex.ecb.data.money;

import java.math.BigDecimal;
import java.math.MathContext;

import me.forex.ecb.data.CurrencyUnit;

/**
 * Created by Dmitry on 28.10.2016.
 * Money class Uses BigDecimal as a storage. In this test no needed to use it.
 */

public class BigMoney extends MoneyImpl {

    private BigDecimal mAmount;

    BigMoney(double amount, CurrencyUnit currency) {
        super(currency);
        mAmount = new BigDecimal(amount, MathContext.DECIMAL64).stripTrailingZeros();
    }

    BigMoney(String amount, CurrencyUnit currency) {
        super(currency);
        mAmount = new BigDecimal(amount);
    }

    BigMoney(BigDecimal amount, CurrencyUnit currency) {
        super(currency);
        mAmount = amount;
    }

    @Override
    protected Money add(FastMoney money) {
        return money.add(this);
    }

    @Override
    public String getAmountString() {
        return mAmount.toPlainString();
    }

    @Override
    public double toDouble() {
        return mAmount.doubleValue();
    }

    @Override
    public BigDecimal toBigDecimal() {
        return mAmount;
    }

    @Override
    public Money negate() {
        return new BigMoney(mAmount.negate(), mCurrency);
    }

    @Override
    public boolean isZero() {
        return mAmount.compareTo(BigDecimal.ZERO) == 0;
    }

    @Override
    public Money multiply(long multiplier) {
        final BigDecimal res = mAmount.multiply(BigDecimal.valueOf(multiplier));
        return MoneyFactory.from(res, mCurrency);
    }

    @Override
    public Money multiply(double multiplier) {
        return MoneyFactory.from(
                mAmount.multiply(new BigDecimal(multiplier, MathContext.DECIMAL64), MathContext.DECIMAL64), mCurrency);
    }

    @Override
    public boolean isNegative() {
        return mAmount.signum() < 0;
    }

    /**
     * Convert current money to another currency with rate
     *
     * @throws IllegalArgumentException
     */
    @Override
    public Money convertTo(CurrencyUnit currency, BigDecimal rate) {
        return MoneyFactory.from(mAmount.multiply(rate), currency);
    }

    @Override
    protected int compareTo(FastMoney other) {
        return -(other.compareTo(this));
    }

}
