package me.forex.ecb.data.money;

import java.math.BigDecimal;
import java.math.BigInteger;

import me.forex.ecb.data.CurrencyUnit;
import me.forex.ecb.utils.CurrencyUtil;

/**
 * Created by Dmitry on 27.10.2016.
 * <p>
 * Class for Money. Precision is usually based on the smallest tick in your exchange data.
 */

public final class FastMoney extends MoneyImpl {

    //in the smallest currency units (f.e. in cents)
    private long mAmount;

    FastMoney(long amount, CurrencyUnit currency) {
        super(currency);
        mAmount = amount;
    }

    /**
     * Convert to the original currency.
     *
     * @return original currency
     */
    @Override
    public double toDouble() {
        return ((double) mAmount) / mCurrency.getMultiplier();
    }

    /**
     * Check negative amount
     *
     * @return true - if negative, false - otherwise
     */
    @Override
    public boolean isNegative() {
        return mAmount < 0;
    }

    /**
     * Return this value with an opposite sign.
     *
     * @return A new object with the same value with a different sign
     */
    @Override
    public Money negate() {
        return new FastMoney(-mAmount, mCurrency);
    }

    @Override
    public boolean isZero() {
        return mAmount == 0;
    }

    @Override
    public BigDecimal toBigDecimal() {
        return BigDecimal.valueOf(mAmount, mCurrency.getDefaultFractionDigits());
    }

    /**
     * Add other Money to current.
     * Important: Money should contains the same currency as this.
     *
     * @param money Money to add.
     * @return new instance Money
     */
    @Override
    public Money add(FastMoney money) {
        checkMoney(money);

        long sum = mAmount + money.mAmount;
        //overflow check
        boolean overflow = mAmount >= 0 && money.mAmount >= 0 && sum < 0;
        return overflow ? money.add(new BigMoney(toBigDecimal(), mCurrency)) : new FastMoney(sum, mCurrency);
    }

    @Override
    public String getAmountString() {
        int precision = mCurrency.getDefaultFractionDigits();
        int part = (int) (mAmount % mCurrency.getMultiplier());
        if (precision == 0 || part == 0) {
            return String.format("%.0f", toDouble());
        }

        final char[] buf = new char[MoneyFactory.MAX_LONG_LENGTH + 3];
        int p = buf.length;
        int remainingPrecision = precision;
        long units = Math.abs(mAmount);
        long q;
        int rem;
        while (remainingPrecision > 0 && units > 0) {
            q = units / 10;
            rem = (int) (units - q * 10);  //avoiding direct % call
            buf[--p] = (char) ('0' + rem);
            units = q;
            --remainingPrecision;
        }
        if (units == 0 && remainingPrecision == 0) {
            //just add "0."
            buf[--p] = '.';
            buf[--p] = '0';
        } else if (units == 0) {
            //some precision left
            while (remainingPrecision > 0) {
                buf[--p] = '0';
                --remainingPrecision;
            }
            buf[--p] = '.';
            buf[--p] = '0';
        } else if (remainingPrecision == 0) {
            //some value left
            buf[--p] = '.';
            while (units > 0) {
                q = units / 10;
                rem = (int) (units - q * 10);
                buf[--p] = (char) ('0' + rem);
                units = q;
            }
        }
        if (mAmount < 0) {
            buf[--p] = '-';
        }
        return new String(buf, p, buf.length - p);
    }

    @Override
    public Money convertTo(CurrencyUnit currency, BigDecimal rate) {
        final long amount = Math.round(mAmount * rate.doubleValue());
        return new FastMoney(amount, currency);
    }

    /**
     * Multiply the current money by the multiplier value.
     *
     * @param multiplier Multiplier
     * @return new instance Money
     */
    @Override
    public Money multiply(long multiplier) {
        final long sum = mAmount * multiplier;

        // overflow check
        final long origAmount = sum / multiplier;
        boolean overflow = origAmount != mAmount;

        if (overflow) {
            final BigInteger res = BigInteger.valueOf(mAmount).multiply(BigInteger.valueOf(multiplier));
            return MoneyFactory.from(new BigDecimal(res), mCurrency);
        } else {
            return new FastMoney(sum, mCurrency);
        }
    }

    /**
     * Multiply the current money by the multiplier value.
     *
     * @param multiplier Multiplier
     * @return new instance Money
     */
    @Override
    public Money multiply(double multiplier) {
        final double unscaledAmount = mAmount * multiplier;
        //round to long - i think it will be enough for precision
        final long result = Math.round(unscaledAmount);
        return new FastMoney(result, mCurrency);
    }

    private void checkMoney(Money money) {
        CurrencyUtil.checkNull(money, "Money can't be null");
        if (mCurrency != money.getCurrency()) {
            throw new IllegalArgumentException("Currency should be equals. "
                    + mCurrency.name() + " is not equals " + money.getCurrency().name());
        }
    }

    @Override
    public int compareTo(FastMoney other) {
        checkMoney(other);
        return compare(mAmount, other.mAmount);
    }

    private static int compare(final long x, final long y) {
        return (x < y) ? -1 : ((x == y) ? 0 : 1);
    }
}
