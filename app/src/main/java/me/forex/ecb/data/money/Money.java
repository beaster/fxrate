package me.forex.ecb.data.money;

import java.math.BigDecimal;

import me.forex.ecb.data.CurrencyUnit;

/**
 * Created by Dmitry on 28.10.2016.
 */

public interface Money extends Comparable<Money> {

    /**
     * Get currency of money
     */
    CurrencyUnit getCurrency();

    /**
     * Convert to the original currency
     */
    double toDouble();

    /**
     * Convert this value into a BigDecimal.
     *
     * @return This object as BigDecimal
     */
    BigDecimal toBigDecimal();

    /**
     * Add another Money object to this one.
     *
     * @param other Other Money object
     * @return A new instance Money object
     */
    Money add(final Money other);

    /**
     * Return this value with an opposite sign.
     *
     * @return A new instance Money with the same value and with a different sign
     */
    Money negate();

    boolean isZero();

    /**
     * Subtract another Money object from this one.
     *
     * @param other Other money object
     * @return A new instance Money
     */
    Money subtract(final Money other);

    /**
     * Multiply the current object.
     *
     * @param multiplier Multiplier
     * @return A new instance Money
     */
    Money multiply(long multiplier);

    /**
     * Multiply the current object.
     *
     * @param multiplier Multiplier
     * @return A new instance Money
     */
    Money multiply(double multiplier);

    /**
     * Return true if amount less than zero
     *
     * @return
     */
    boolean isNegative();

    /**
     * Convert to anther money by rate
     *
     * @param currency
     * @param rate
     * @return
     */
    Money convert(CurrencyUnit currency, BigDecimal rate);

    /**
     * Return amount string with a decimal dot, e.q. 3.34, 0.22 etc.
     * Precision depends on current currency.
     *
     * @return amount string
     */
    String getAmountString();


}
