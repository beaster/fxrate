package me.forex.ecb.data.money;

import java.math.BigDecimal;
import java.math.BigInteger;

import me.forex.ecb.data.CurrencyUnit;
import me.forex.ecb.utils.CurrencyUtil;

/**
 * Created by Dmitry on 28.10.2016.
 * Factory for Money instances. Precision is based on smallest tick in your exchange data.
 * Support as FastMoney(Long) and as BigMoney (BigDecimal). Support only 15 digital precision.
 * It's enough for this.
 */

public class MoneyFactory {

    static final int MAX_LONG_LENGTH = Long.toString(Long.MAX_VALUE).length();
    static final int MAX_PRECISION = 15;

    /**
     * Convert String monetary value into Money. Supported format 12.55, 444,555.
     * Separate decimal part can be dot or comma.
     *
     * @param amount   Amount to parse
     * @param currency Currency for amount
     * @return Money instance
     * @throws java.lang.IllegalArgumentException In case of any errors
     */
    public static Money from(String amount, CurrencyUnit currency) {
        CurrencyUtil.checkNull(currency, "Currency can't be null");
        CurrencyUtil.checkNull(amount, "Amount can't be null");

        return fromString(amount, currency);
    }

    /**
     * Convert double monetary value into Money.
     *
     * @param amount   double value to parse
     * @param currency Currency for amount
     * @return Money instance
     * @throws java.lang.IllegalArgumentException In case of any errors
     */
    public static Money from(double amount, CurrencyUnit currency) {
        CurrencyUtil.checkNull(currency, "Currency can't be null");

        final double multiplied = amount * currency.getMultiplier();
        final long converted = (long) multiplied;
        return multiplied != converted ? new BigMoney(amount, currency)
                : new FastMoney(converted, currency);
    }

    /**
     * Convert BigDecimal monetary value into Money.
     *
     * @param amount   BigDecimal value to parse
     * @param currency Currency for amount
     * @return Money instance
     * @throws java.lang.IllegalArgumentException In case of any errors
     */
    public static Money from(BigDecimal amount, CurrencyUnit currency) {
        CurrencyUtil.checkNull(currency, "Currency can't be null");
        CurrencyUtil.checkNull(amount, "Amount can't be null");

        if (amount.getClass() != BigDecimal.class) {
            BigInteger value = amount.unscaledValue();
            if (value == null) {
                throw new IllegalArgumentException("Illegal BigDecimal subclass");
            }
            if (value.getClass() != BigInteger.class) {
                value = new BigInteger(value.toString());
            }
            amount = new BigDecimal(value, amount.scale());
        }
        return new BigMoney(amount, currency);
    }

    private static Money fromString(String value, CurrencyUnit currency) {
        final int dotPos = value.indexOf('.');
        final int precision = dotPos == -1 ? 0 : value.length() - dotPos - 1;

        if (dotPos != -1 && value.indexOf('.', dotPos + 1) != -1) {
            throw new IllegalArgumentException("Amount has more than 1 decimal point: " + value);
        }
        try {
            String total = value;
            long amount;
            if (precision > currency.getDefaultFractionDigits()) {
                total = value.substring(0, dotPos + currency.getDefaultFractionDigits() + 1);
            }
            amount = Long.parseLong(total.replace(".", ""));

            if (currency.getDefaultFractionDigits() > precision) {
                amount = (long) (amount * Math.pow(10.0, currency.getDefaultFractionDigits() - precision));
            }

            return new FastMoney(amount, currency);
        } catch (NumberFormatException ex) {
            try {
                return new BigMoney(value, currency);
            } catch (NumberFormatException ex2) {
                throw new IllegalArgumentException("Unparseable value provided: " + value, ex2);
            }
        }
    }

}
