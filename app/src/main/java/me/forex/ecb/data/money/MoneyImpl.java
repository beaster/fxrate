package me.forex.ecb.data.money;

import java.math.BigDecimal;
import java.math.MathContext;

import me.forex.ecb.data.CurrencyUnit;
import me.forex.ecb.utils.CurrencyUtil;

/**
 * Created by Dmitry on 28.10.2016.
 */

public abstract class MoneyImpl implements Money {

    protected CurrencyUnit mCurrency;

    MoneyImpl(CurrencyUnit currency) {
        mCurrency = currency;
    }

    @Override
    public CurrencyUnit getCurrency() {
        return mCurrency;
    }

    @Override
    public Money add(final Money other) {
        if (other instanceof FastMoney) {
            return add((FastMoney) other);
        } else {
            return add((BigMoney) other);
        }
    }

    protected abstract Money add(final FastMoney money);

    protected Money add(final BigMoney money) {
        CurrencyUtil.checkNull(money, "Money can't be null");
        final BigDecimal res = toBigDecimal().add(money.toBigDecimal(), MathContext.DECIMAL128);
        return MoneyFactory.from(res, money.getCurrency());
    }

    @Override
    public Money subtract(final Money other) {
        return add(other.negate());
    }

    /**
     * Return amount string with sign symbol, a decimal dot and currency symbol. For example $1.23, -$4.54
     * Precision depends on current currency.
     *
     * @return amount string
     */
    public String getFullAmountString() {
        StringBuilder sb = new StringBuilder();
        if (isNegative()) {
            sb.append("-");
        }

        String plainAmount = getAmountString();
        plainAmount = plainAmount.replaceAll("-", "");
        sb.append(mCurrency.getSymbol()).append(plainAmount);

        return sb.toString();
    }

    /**
     * Convert current money to another currency with rate
     *
     * @throws IllegalArgumentException
     */
    @Override
    public Money convert(CurrencyUnit currency, BigDecimal rate) {
        CurrencyUtil.checkNull(currency, "Currency cant't be null");
        CurrencyUtil.checkNull(rate, "Rate cant't be null");

        if (getCurrency() == currency) {
            if (rate.compareTo(BigDecimal.ONE) == 0) {
                return this;
            }
            throw new IllegalArgumentException("Cannot convert to the same currency");
        }
        if (rate.compareTo(BigDecimal.ZERO) < 0) {
            throw new IllegalArgumentException("Cannot convert using a negative conversion multiplier");
        }

        return convertTo(currency, rate);
    }

    public abstract Money convertTo(CurrencyUnit toCurrency, BigDecimal rate);

    @Override
    public int compareTo(final Money other) {
        if (other instanceof FastMoney) return compareTo((FastMoney) other);
        else return compareTo((BigMoney) other);
    }

    protected abstract int compareTo(final FastMoney other);

    public int compareTo(final BigMoney other) {
        return toBigDecimal().compareTo(other.toBigDecimal());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        if (!(obj instanceof Money)) {
            return false;
        }

        Money other = (Money) obj;

        return getCurrency() == other.getCurrency();
    }
}
