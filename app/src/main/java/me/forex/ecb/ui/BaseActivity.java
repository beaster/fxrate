package me.forex.ecb.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import me.forex.ecb.R;

/**
 * Created by Dmitry on 26.10.2016.
 */

public class BaseActivity extends AppCompatActivity {

    private TextView mTitle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_activity);
        setupViews();
    }

    private void setupViews() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);

        mTitle = (TextView) findViewById(R.id.title);
    }

    public void setTitleToolbar(CharSequence text) {
        mTitle.setText(text);
    }

    public void setVisibilityToolBar(int visibility) {
        mTitle.setVisibility(visibility);
    }

    public void addFragment(Fragment fragment) {
        boolean noFragment = getSupportFragmentManager()
                .findFragmentById(R.id.contentFrame) == null;
        if (noFragment) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.contentFrame, fragment);
            transaction.commit();
        }
    }

}
