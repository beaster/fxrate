package me.forex.ecb.ui.exchange;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import me.forex.ecb.R;
import me.forex.ecb.data.Wallet;
import me.forex.ecb.data.money.Money;
import me.forex.ecb.ui.view.EndlessPageAdapter;
import me.forex.ecb.ui.view.MoneyView;
import me.forex.ecb.ui.view.WalletView;

/**
 * Created by Dmitry on 29.10.2016.
 */

public class EndlessExchangePageAdapter extends ViewPageAdapter implements EndlessPageAdapter {

    private int realSize;
    private List<Money> mData;

    private OnStateChangeListener mStateListener;

    EndlessExchangePageAdapter(Wallet wallet, OnStateChangeListener listener) {
        mData = wallet.getMoneyList();
        realSize = mData.size();
        mStateListener = listener;
    }

    @Override
    public View getView(int position, ViewPager pager) {
        int viewPosition = position % realSize;

        MoneyView moneyView = newView(pager.getContext());
        moneyView.setOnStateChangeListener(mStateListener);
        moneyView.setTag(position);
        moneyView.fill(mData.get(viewPosition), ((WalletView) pager.getParent()).isPrimary());

        return moneyView;
    }

    public MoneyView newView(Context context) {
        return (MoneyView) LayoutInflater.from(context).inflate(R.layout.money_view, null);
    }

    @Override
    public int getCount() {
        return realSize > 0 ? Integer.MAX_VALUE : 0;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        int virtualPosition = position % realSize;
        super.destroyItem(container, virtualPosition, object);
    }

    @Override
    public int getRealCount() {
        return realSize;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}
