package me.forex.ecb.ui.exchange;

import android.os.Bundle;
import android.support.annotation.Nullable;

import me.forex.ecb.ui.BaseActivity;

/**
 * Created by Dmitry on 26.10.2016.
 */

public class ExchangeActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        addFragment(ExchangeFragment.getInstance());
    }
}
