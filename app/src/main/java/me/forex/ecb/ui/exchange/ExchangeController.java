package me.forex.ecb.ui.exchange;

import android.support.v4.view.ViewPager;

import me.forex.ecb.data.Wallet;
import me.forex.ecb.data.money.Money;
import me.forex.ecb.ui.view.MoneyView;
import me.forex.ecb.ui.view.WalletView;

/**
 * Created by Dmitry on 31.10.2016.
 * Controller to provide state of money to caller
 */

public class ExchangeController implements OnStateChangeListener {

    public final static int TYPE_FOCUS = 1;
    public final static int TYPE_TYPING = 2;

    /**
     * Listener to get all state from wallets
     */
    public interface OnMoneyStateChangeListener {
        /**
         * Notify any changes from wallets to caller (Focus change, page change, typing)
         *
         * @param from         primary current MoneyView
         * @param from         second current MoneyView
         * @param typeOfChange type of notify. Can be TYPE_FOCUS,TYPE_TYPING.
         */
        void onMoneyStateChange(MoneyView from, MoneyView to, int typeOfChange);
    }

    private WalletView mFrom;
    private WalletView mTo;
    private OnMoneyStateChangeListener mListener;
    private Wallet mWallet;

    public ExchangeController(WalletView from, WalletView to) {
        mFrom = from;
        mTo = to;
        mWallet = Wallet.getDefault();
        init();
    }

    private void init() {
        mFrom.setAdapter(new EndlessExchangePageAdapter(mWallet, this));
        mFrom.setPrimary(true);
        mFrom.getViewPager().setCurrentItem(0);
        mTo.setAdapter(new EndlessExchangePageAdapter(mWallet, this));
        mFrom.getViewPager().setCurrentItem(1);

        initPageListener(mFrom);
        initPageListener(mTo);
    }

    private void initPageListener(WalletView view) {
        view.getViewPager().addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                notifyChange(TYPE_FOCUS);
            }
        });
    }

    private void notifyChange(int type) {
        if (mListener != null) {
            if (hasFocusableView(mFrom) || hasFocusableView(mTo)) {
                mListener.onMoneyStateChange(mFrom.getCurrentMoneyView(), mTo.getCurrentMoneyView(), type);
            }
        }
    }

    private boolean hasFocusableView(WalletView view) {
        return view != null && view.getCurrentMoneyView() != null ? view.getCurrentMoneyView().hasEditTextFocus() : false;
    }

    public void exchangeCurrent() {
        mWallet.changeMoney(exchange(mFrom));
        mWallet.changeMoney(exchange(mTo));

        mWallet.save();
        //update ui
        invalidateInternal(mFrom);
        invalidateInternal(mTo);
    }

    private void invalidateInternal(WalletView view) {
        view.getViewPager().getAdapter().notifyDataSetChanged();
    }

    private Money exchange(WalletView walletView) {
        Money money = walletView.getCurrentMoneyView().getMoney();
        Money inputMoney = walletView.getCurrentMoneyView().getInputMoney();
        return money.add(inputMoney);
    }

    /**
     * Set listener to get all changes from wallets
     *
     * @param listener
     */
    public void setOnMoneyStateChangeListener(OnMoneyStateChangeListener listener) {
        mListener = listener;
    }

    @Override
    public void onFocusChange(boolean hasFocus) {
        if (hasFocus) {
            notifyChange(TYPE_FOCUS);
        }
    }

    @Override
    public void onTextChange(String text) {
        notifyChange(TYPE_TYPING);
    }

}
