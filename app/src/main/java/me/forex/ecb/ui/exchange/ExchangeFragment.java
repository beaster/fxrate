package me.forex.ecb.ui.exchange;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import me.forex.ecb.R;
import me.forex.ecb.conversion.ConversionRate;
import me.forex.ecb.conversion.ConversionRateManager;
import me.forex.ecb.conversion.ConversionSyncHelper;
import me.forex.ecb.data.money.Money;
import me.forex.ecb.data.money.MoneyFactory;
import me.forex.ecb.data.money.MoneyImpl;
import me.forex.ecb.ui.BaseActivity;
import me.forex.ecb.ui.BaseFragment;
import me.forex.ecb.ui.view.MoneyView;
import me.forex.ecb.ui.view.WalletView;

/**
 * Created by Dmitry on 26.10.2016.
 */

public class ExchangeFragment extends BaseFragment implements
        ConversionSyncHelper.OnUpdateRateListener, ExchangeController.OnMoneyStateChangeListener {

    private WalletView mFrom;
    private WalletView mTo;

    private ConversionSyncHelper mSyncHelper;
    private ExchangeController mController;

    public static ExchangeFragment getInstance() {
        return new ExchangeFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSyncHelper = new ConversionSyncHelper();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.exchange_fragment, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFrom = (WalletView) view.findViewById(R.id.from);
        mTo = (WalletView) view.findViewById(R.id.to);

        mController = new ExchangeController(mFrom, mTo);
        setVisibilityToolBar(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
        mController.setOnMoneyStateChangeListener(this);
        mSyncHelper.setOnRateUpdateListener(this);
        mSyncHelper.startSync();
    }

    @Override
    public void onPause() {
        super.onPause();
        mSyncHelper.stopSync();
        mSyncHelper.setOnRateUpdateListener(null);
        mController.setOnMoneyStateChangeListener(null);

    }

    private void changeTitle(CharSequence text) {
        setVisibilityToolBar(View.VISIBLE);
        ((BaseActivity) getActivity()).setTitleToolbar(text);
    }

    private void setVisibilityToolBar(int visibility) {
        ((BaseActivity) getActivity()).setVisibilityToolBar(visibility);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_exchange, menu);
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem item = menu.findItem(R.id.action_exchange);
        item.setEnabled(canExchange());
    }

    private boolean canExchange() {
        boolean exchange = false;
        if (mFrom.getCurrentMoneyView() != null && mTo.getCurrentMoneyView() != null) {
            exchange = mFrom.getCurrentMoneyView().canExchange() && mTo.getCurrentMoneyView().canExchange();
        }
        return exchange;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_exchange) {
            exchange();
        }
        return super.onOptionsItemSelected(item);
    }

    private void exchange() {
        mController.exchangeCurrent();
        Toast.makeText(getContext(), R.string.toast_success, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUpdate(ConversionRateManager.Conversion rate) {
        //we have new information about rate
        //1) Need to update rate in title
        //2) Need to update rate in money
        updateInternal(mFrom.getCurrentMoneyView(), mTo.getCurrentMoneyView(), rate, ExchangeController.TYPE_FOCUS);
    }

    private void updateInternal(MoneyView primary, MoneyView second, ConversionRateManager.Conversion rate, int type) {
        if (primary != null && second != null && (primary.hasEditTextFocus() || second.hasEditTextFocus())) {
            MoneyView fromView = primary.hasEditTextFocus() ? primary : second;
            MoneyView toView = !primary.hasEditTextFocus() ? primary : second;

            Money from = fromView.getMoney();
            Money to = toView.getMoney();

            boolean isEquals = from.getCurrency() == to.getCurrency();
            if (isEquals || rate == null) {
                setVisibilityToolBar(View.GONE);
                if (type == ExchangeController.TYPE_FOCUS) {
                    toView.clear();
                }
            } else {
                //show current rate in title
                ConversionRate conversion = rate.getRate(from.getCurrency(), to.getCurrency());
                changeTitle(conversion.toString());

                //show current rate in views if needed
                ConversionRate inverseConversion = rate.getRate(to.getCurrency(), from.getCurrency());
                fromView.hideRate();
                toView.showRate(inverseConversion);

                //update amount
                String amount = fromView.getInputText();
                boolean clear = true;
                if (!TextUtils.isEmpty(amount)) {
                    Money money = MoneyFactory.from(amount, from.getCurrency());
                    if (!money.isZero()) {
                        amount = ((MoneyImpl) money)
                                .convertTo(to.getCurrency(), conversion.getRate()).getAmountString();
                        fromView.showSign();
                        toView.showSign();
                        clear = false;
                    }
                }

                if (clear) {
                    fromView.hideSign();
                    toView.setInputText("");
                } else {
                    toView.setInputText(amount);
                }

            }
        }
        getActivity().invalidateOptionsMenu();
    }

    @Override
    public void onMoneyStateChange(MoneyView from, MoneyView to, int type) {
        updateInternal(from, to, mSyncHelper.getRateManager().getCurrentRate(), type);
    }
}
