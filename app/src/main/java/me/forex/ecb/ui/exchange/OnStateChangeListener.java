package me.forex.ecb.ui.exchange;

/**
 * Created by Dmitry on 31.10.2016.
 * Listener for internal usage
 */

public interface OnStateChangeListener {

    void onFocusChange(boolean hasFocus);

    void onTextChange(String text);
}
