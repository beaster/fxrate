package me.forex.ecb.ui.exchange;

import android.support.v4.view.ViewPager;

/**
 * Created by Dmitry on 30.10.2016.
 */

public class SimplePageChangeListener implements ViewPager.OnPageChangeListener {

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
