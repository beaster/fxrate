package me.forex.ecb.ui.view;

/**
 * Created by Dmitry on 29.10.2016.
 */

public interface EndlessPageAdapter {
    int getRealCount();

}
