package me.forex.ecb.ui.view;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;

/**
 * Created by Dmitry on 29.10.2016.
 */

public class EndlessViewPager extends ViewPager {

    public EndlessViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setAdapter(PagerAdapter adapter) {
        super.setAdapter(adapter);
        setCurrentItem(0);
    }

    @Override
    public void setCurrentItem(int item) {
        item = getOffsetAmount() + (item % getAdapter().getCount());
        super.setCurrentItem(item);

    }

    private int getOffsetAmount() {
        if (getAdapter() instanceof EndlessPageAdapter) {
            EndlessPageAdapter infAdapter = (EndlessPageAdapter) getAdapter();
            return infAdapter.getRealCount() * 1000;
        } else {
            return 0;
        }
    }
}
