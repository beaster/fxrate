package me.forex.ecb.ui.view;

import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Dmitry on 29.10.2016.
 */

public class MoneyEditText extends EditText {

    public interface OnEmptyModeListener {
        void onEmptyModeEnable();
    }

    public interface OnTextChangeListener {
        void onTextChanged(String text);
    }


    public static class DecimalDigitsInputFilter implements InputFilter {
        private Pattern mPattern;

        public DecimalDigitsInputFilter(int precision) {
            mPattern = Pattern.compile("-?[0-9]+((\\.[0-9]{0," + (precision)
                    + "})?)||(\\.)?");
        }

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            String replacement = source.subSequence(start, end).toString();
            String newVal = dest.subSequence(0, dstart).toString() + replacement
                    + dest.subSequence(dend, dest.length()).toString();
            Matcher matcher = mPattern.matcher(newVal);
            if (matcher.matches()) {
                return null;
            }

            if (TextUtils.isEmpty(source)) {
                return dest.subSequence(dstart, dend);
            } else {
                return "";
            }
        }
    }

    private OnEmptyModeListener mListener;
    private OnTextChangeListener mTextListener;
    private TextWatcher mWatcher;

    private boolean isEmptyMode;

    public MoneyEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        addTextChangedListener(mWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(s.toString())) {
                    applyEmptyMode();
                } else {
                    if (isEmptyMode) {
                        isEmptyMode = false;
                        s.delete(s.length() - 1, s.length());
                    }
                }
                if (mTextListener != null) {
                    mTextListener.onTextChanged(s.toString());
                }
            }
        });
    }

    void setOnEmptyModeListener(OnEmptyModeListener listener) {
        mListener = listener;
    }

    void setOnTextChangeListener(OnTextChangeListener listener) {
        mTextListener = listener;
    }

    void applyEmptyMode() {
        if (!isEmptyMode) {
            isEmptyMode = true;
            removeTextChangedListener(mWatcher);
            setText("0");
            addTextChangedListener(mWatcher);
            if (mListener != null) {
                mListener.onEmptyModeEnable();
            }
        }
    }

    public boolean isEmptyMode() {
        return isEmptyMode;
    }

    @Override
    protected void onSelectionChanged(int selStart, int selEnd) {
        if (isEmptyMode) {
            setSelection(this.length() - 1);
        }
    }

}
