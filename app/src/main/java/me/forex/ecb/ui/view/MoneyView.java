package me.forex.ecb.ui.view;

import android.content.Context;
import android.graphics.Color;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import me.forex.ecb.R;
import me.forex.ecb.conversion.ConversionRate;
import me.forex.ecb.data.money.Money;
import me.forex.ecb.data.money.MoneyFactory;
import me.forex.ecb.data.money.MoneyImpl;
import me.forex.ecb.ui.exchange.OnStateChangeListener;

/**
 * Created by Dmitry on 29.10.2016.
 */

public class MoneyView extends RelativeLayout implements MoneyEditText.OnEmptyModeListener,
        MoneyEditText.OnTextChangeListener {

    private TextView mCurrencyNameTextView;
    private MoneyEditText mEditText;
    private TextView mSignTextView;
    private TextView mTotalTextView;
    private TextView mRateTextView;

    private boolean isPrimary;
    private Money mMoney;
    private OnStateChangeListener mListener;
    private boolean canExchange = true;

    public MoneyView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mCurrencyNameTextView = (TextView) findViewById(R.id.currency_name);
        mEditText = (MoneyEditText) findViewById(R.id.amount_edit_text);
        mSignTextView = (TextView) findViewById(R.id.sign);
        mTotalTextView = (TextView) findViewById(R.id.total);
        mRateTextView = (TextView) findViewById(R.id.rate);
    }

    public boolean hasEditTextFocus() {
        return mEditText.hasFocus();
    }

    public void setOnStateChangeListener(final OnStateChangeListener listener) {
        mListener = listener;
    }

    public void fill(Money money, boolean isPrimary) {
        mMoney = money;
        this.isPrimary = isPrimary;

        mCurrencyNameTextView.setText(money.getCurrency().name());
        mEditText.applyEmptyMode();
        mEditText.setFilters(new InputFilter[]{
                new InputFilter.LengthFilter(8), //TODO 8?
                new MoneyEditText.DecimalDigitsInputFilter(money.getCurrency().getDefaultFractionDigits())
        });
        mEditText.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (mListener != null) {
                    mListener.onFocusChange(hasFocus);
                }
            }
        });
        mEditText.setOnEmptyModeListener(this);
        mEditText.setOnTextChangeListener(this);

        String amount = ((MoneyImpl) money).getFullAmountString();
        amount = String.format(getResources().getString(R.string.total_amount), amount);
        mTotalTextView.setText(amount);

        mSignTextView.setText(isPrimary ? "-" : "+");
        mSignTextView.setVisibility(GONE);
    }

    public Money getMoney() {
        return mMoney;
    }

    public Money getInputMoney() {
        String amount = TextUtils.isEmpty(getInputText()) ? "0" : getInputText();
        if (isPrimary) {
            amount = "-" + amount;
        }
        return MoneyFactory.from(amount, mMoney.getCurrency());
    }

    public String getInputText() {
        return mEditText.isEmptyMode() ? "" : mEditText.getText().toString();
    }

    public void setInputText(String text) {
        mEditText.setOnTextChangeListener(null);
        updateColor(text);
        if (TextUtils.isEmpty(text)) {
            mEditText.applyEmptyMode();
        } else {
            mEditText.setText(text);
        }
        mEditText.setOnTextChangeListener(this);
    }

    public void showSign() {
        mSignTextView.setVisibility(VISIBLE);
    }

    public void hideSign() {
        mSignTextView.setVisibility(GONE);
    }

    public void showRate(ConversionRate rate) {
        mRateTextView.setText(rate.toString());
        mRateTextView.setVisibility(VISIBLE);
    }

    public void hideRate() {
        mRateTextView.setVisibility(GONE);
    }

    public void clear() {
        hideRate();
        mEditText.applyEmptyMode();
    }

    @Override
    public void onEmptyModeEnable() {
        hideSign();
    }

    @Override
    public void onTextChanged(String text) {
        updateColor(text);
        if (mListener != null) {
            mListener.onTextChange(text);
        }
    }

    private void updateColor(String text) {
        if (isPrimary) {
            if (!TextUtils.isEmpty(text)) {
                Money money = MoneyFactory.from(text, mMoney.getCurrency());
                canExchange = mMoney.compareTo(money) >= 0;
            }
            mTotalTextView.setTextColor(canExchange ? Color.WHITE : Color.RED);
        }
    }

    public boolean canExchange() {
        return canExchange;
    }
}
