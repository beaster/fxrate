package me.forex.ecb.ui.view;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import me.forex.ecb.R;

/**
 * Created by Dmitry on 29.10.2016.
 */

public class WalletView extends LinearLayout {

    private ViewPager mViewPager;
    private EndlessCirclePageIndicator mIndicator;
    private boolean isPrimary;

    public WalletView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        init();
    }

    private void init() {
        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        mViewPager.setOffscreenPageLimit(1);
        mIndicator = (EndlessCirclePageIndicator) findViewById(R.id.indicator);
    }

    public void setAdapter(PagerAdapter adapter) {
        mViewPager.setAdapter(adapter);
        mIndicator.setViewPager(mViewPager);
    }

    public void setPrimary(boolean isPrimary) {
        this.isPrimary = isPrimary;
    }

    public boolean isPrimary() {
        return isPrimary;
    }

    public ViewPager getViewPager() {
        return mViewPager;
    }

    public MoneyView getCurrentMoneyView() {
        return (MoneyView) mViewPager.findViewWithTag(mViewPager.getCurrentItem());
    }

}
