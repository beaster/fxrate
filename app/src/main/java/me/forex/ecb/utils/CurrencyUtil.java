package me.forex.ecb.utils;

import java.math.BigDecimal;

/**
 * Created by Dmitry on 26.10.2016.
 */

public final class CurrencyUtil {

    private CurrencyUtil() {

    }

    public static double round(double value, int scale) {
        return new BigDecimal(String.valueOf(value)).setScale(scale, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    public static void checkNull(Object value, String error) {
        if (value == null) {
            throw new IllegalArgumentException(error);
        }
    }

}
