package me.forex.ecb;

import junit.framework.Assert;

import org.junit.Test;

import me.forex.ecb.data.CurrencyUnit;
import me.forex.ecb.data.money.Money;
import me.forex.ecb.data.money.MoneyFactory;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    //TODO: Need to add test

    @Test
    public void testMoney() throws Exception {
        Money money = MoneyFactory.from("29.05", CurrencyUnit.USD);
        Assert.assertEquals(money.getAmountString(), "29.05");
        Assert.assertFalse(money.isNegative());
        Assert.assertFalse(money.isZero());
        Assert.assertEquals(money.getCurrency(), CurrencyUnit.USD);
        Assert.assertEquals(money.toDouble(), 29.05);
        Assert.assertEquals(money.negate().getAmountString(), "-29.05");
    }

    @Test
    public void testMoneyOperation() throws Exception {
        Money money = MoneyFactory.from("29.05", CurrencyUnit.USD);
        Money money1 = MoneyFactory.from("18", CurrencyUnit.USD);

        Assert.assertEquals(money.add(money1).toDouble(), 47.05);
        Assert.assertEquals(money.add(money1.negate()).toDouble(), 11.05);
    }
}